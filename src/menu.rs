use crate::{Scene, Window};
use crate::geometry::Rectangle;
use crate::types::N;

use kiss3d::{scene::PlanarSceneNode, text::Font};
use nalgebra::{Vector2, Isometry2, Point2, Point3, Translation2};

use std::rc::Rc;


struct Item {
    pos: Translation2<N>,
    text: String,
    poly: PlanarSceneNode,
    color: (f32, f32, f32)
}

pub struct Menu {
    items: Vec<Item>,
    font: Rc<Font>
}

impl Menu {
    pub fn new(w: &mut Window, font: Rc<Font>) -> Menu {
        let rect = Rectangle::new((-100, -20), (100, 20));
        let points = rect.points();
        let start = Item {
            pos: Translation2::new(0.0, 60.0),
            poly: w.add_convex_polygon(points.clone(), Vector2::new(1.0, 1.0)),
            text: String::from("Start"),
            color: (0.1, 0.2, 0.1)
        };
        let code = Item {
            pos: Translation2::new(0.0, -60.0),
            poly: w.add_convex_polygon(points, Vector2::new(1.0, 1.0)),
            text: String::from("Code"),
            color: (0.15, 0.15, 0.15)
        };

        Menu {
            font,
            items: vec![
                start,
                code
        ]}
    }
}

fn get_phys_mouse_coors(w: &mut Window) -> Option<(f32, f32)> {
    w.cursor_pos().map(|(x, y)|
        (x as f32 - w.width() as f32 / 2.0, w.height() as f32 / 2.0 - y as f32)
    )
}

impl Scene for Menu {
    fn step(&mut self) -> Option<Box<dyn Scene>> {
        None
    }

    fn render(&mut self, w: &mut Window) {
        let dpi = w.hidpi_factor() as f32;
        for item in self.items.iter_mut() {
            item.poly.set_local_translation(item.pos);
            let (r, g, b) = item.color;
            item.poly.set_color(r, g, b);
            if let Some((mx, my)) = get_phys_mouse_coors(w) {
                let (mx, my) = (mx / dpi, my / dpi);
                println!("Cursor at {} {}", mx, my);
                if (
                    mx >= item.pos.vector[0] - 100.0 && mx <= item.pos.vector[0] + 100.0 &&
                    my >= item.pos.vector[1] - 20.0 && my <= item.pos.vector[1] + 20.0
                )
                    {
                    item.poly.set_color(r + 0.1, g + 0.1, b + 0.1);
                }
            }
            let mut text_pos = Point2::new(
                w.width() as N / 2.0 + item.pos.vector[0] * 2.0,
                w.height() as N / 2.0 - item.pos.vector[1] * 2.0);
            text_pos[0] *= dpi; //
            text_pos[1] *= dpi;
            text_pos[1] += 40.0; // offset for center
           // println!("Text at {:?}, dpi {}", text_pos, dpi);
            w.draw_text(
                &item.text,
                &text_pos,
                60.0,
                &self.font,
                &Point3::new(0.5, 0.5, 0.5)
            )
        }
    }
}