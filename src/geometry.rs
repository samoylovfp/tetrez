use crate::types::N;
use nalgebra::Point2;

#[derive(Clone)]
pub struct Rectangle(pub Vec<(N, N)>);

#[derive(Clone)]
pub struct Shape(pub Vec<Rectangle>);


const GEOMETRY_OVERLAP: N = 0.001;

impl Rectangle {
    pub fn new((x1, y1): (i16, i16), (x2, y2): (i16, i16)) -> Rectangle {
        let m = GEOMETRY_OVERLAP;

        let (x1, y1): (N, N) = (x1 as N - m, y1 as N - m);
        let (x2, y2): (N, N) = (x2 as N + m, y2 as N + m);
        Rectangle(vec![
            (x1, y1),
            (x1, y2),
            (x2, y2),
            (x2, y1)
        ])
    }

    pub fn scale(&mut self, s: N) {
        for c in self.0.iter_mut() { 
            c.0 *= s;
            c.1 *= s;
        }
    }

    pub fn points(&self) -> Vec<Point2<N>> {
        self.0.iter().map(|&(x, y)| Point2::new(x, y)).collect()
    }
}

impl Shape {
    pub fn union_shape(&self) -> Vec<(N, N)> {
        vec![]
    }
    pub fn scale(&mut self, s: N) {
        for r in self.0.iter_mut() {
            r.scale(s);
        }
    }
}

pub fn get_tetros() -> Vec<Shape> {
    [
        [  // Г
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 2)
        ], 
        [  // []
            (0, 0),
            (0, 1),
            (1, 0),
            (1, 1)
        ],
        [  // s
            (0, 0),
            (1, 0),
            (1, 1),
            (2, 1)
        ],
        [  // T
            (0, 1),
            (1, 1),
            (2, 1),
            (1, 0)
        ],
        [  // |
            (0, 0),
            (0, 1),
            (0, 2),
            (0, 3)
        ]
    ].iter().map(|points|
        Shape(points.iter().map(|&(x, y)|
            Rectangle::new((x, y), (x + 1, y + 1))).collect()
        )
    ).collect()
}