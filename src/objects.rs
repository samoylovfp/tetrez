use nalgebra::{
    Point2,
    Isometry2,
    Vector2
};
use nphysics2d::{
    world::World,
    volumetric::volumetric::Volumetric,
    object::Material
};
use kiss3d::{
    window::Window,
    scene::PlanarSceneNode
};
use ncollide2d::{
    shape
};

use crate::geometry::{
    Shape,
    Rectangle
};
use crate::types::{
    N,
    Color
};



pub struct Part {
    pub sprite: PlanarSceneNode,
    pub geometry: Rectangle
}

const UVS: [[f32; 2]; 4] = [
        [0.0, 0.0],
        [0.0, 1.0],
        [1.0, 1.0],
        [1.0, 0.0],
    ];

impl Part {
    fn new(geometry: Rectangle, w: &mut Window, color: Color, i: usize, tex_name: &'static str) -> Part {
        let mut sprite = w.add_convex_polygon(
                geometry.0.iter().cloned().map(|(x, y)| Point2::new(x, y)).collect(),
                Vector2::new(1.0, 1.0)
            );
        sprite.set_color(color.0, color.1, color.2);
        sprite.set_texture_with_name(tex_name);
        sprite.modify_uvs(&mut |points|{
            for(i,p) in points.iter_mut().enumerate() {
                p[0] = UVS[i][0];
                p[1] = UVS[i][1];
            }
        });
        Part {
            sprite,
            geometry
        }
    }
}

pub struct Obj {
    color: (f32, f32, f32),
    geometry: Vec<(N, N)>,
    parts: Vec<Part>,
    rigid_body: nphysics2d::object::BodyHandle
}

impl Obj {
    pub fn new(world: &mut World<N>, window: &mut Window, x: N, y: N, r: f32, g: f32, b: f32, shape: Shape, tex_name: &'static str) -> Obj {
        let parts: Vec<_> = shape.0.iter().enumerate().map(|(i, rect)|
            Part::new(rect.clone(), window, (r, g, b), i, tex_name)
        ).collect();

        let compound_shape = shape::Compound::new(
            parts.iter().filter_map(|p|
                    shape::ConvexPolygon::try_from_points(
                        &p.geometry.0.iter().map(|&(x, y)| Point2::new(x, y)).collect::<Vec<_>>()
                    )
            ).map(|compound| (Isometry2::identity(), shape::ShapeHandle::new(compound))).collect()
        );

        let rigid_body_handle = world.add_rigid_body(
            Isometry2::new(Vector2::new(x, y), nalgebra::zero()),
            compound_shape.inertia(1.0),
            compound_shape.center_of_mass()
        );

        let _collider = world.add_collider(
            0.001,
            shape::ShapeHandle::new(compound_shape),
            rigid_body_handle,
            Isometry2::identity(),
            Material::new(0.3, 0.9),
        );
        Obj {
            geometry: shape.union_shape(),
            color: (r, g, b),
            parts,
            rigid_body: rigid_body_handle
        }
    }
    pub fn update(&mut self, world: &World<N>) {
        if let Some(phys_obj) = world.rigid_body(self.rigid_body) {
            for p in self.parts.iter_mut() {
                p.sprite.set_local_transformation(phys_obj.position())
            }
        }
    }
}