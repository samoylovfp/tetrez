use std::collections::VecDeque;
const FPS_PERIOD: u64 = 1000; // millis

pub struct FPSCounter {
    frames: VecDeque<u64>
}

impl FPSCounter {
    pub fn new() -> FPSCounter {
        FPSCounter{frames: VecDeque::new()}
    }
    pub fn frame(&mut self) {
        self.frames.push_back(now_millis())
    }
    pub fn get_fps(&mut self) -> f32 {
        let now = now_millis();
        while let Some(val) = self.frames.front() {
            let passed_millis = now - val;
            if passed_millis < FPS_PERIOD {
                let frames_time = passed_millis as f32 / 1000.0;
                return self.frames.len() as f32 / frames_time;
            }
            self.frames.pop_front();
        }
        return 0.0;
    }
}


#[cfg(target_arch = "wasm32")]
mod objects {
    pub fn play_sound(volume: f32) {
        js!{
            var audio = new Audio("hit.mp3");
            audio.volume = @{volume};
            audio.play();
        }
    }

    pub fn now_millis() -> u64 {
        use stdweb::unstable::TryInto;
        let millis: u32 = js!{
            return window.performance.now()
        }.try_into().unwrap_or(0);
        millis as u64
    }
}

#[cfg(not(target_arch = "wasm32"))]
mod objects {

    pub fn play_sound(volume: f32) {
        println!("BOINK! {}", volume);
    }

    pub fn now_millis() -> u64 {
        let start = std::time::SystemTime::now();
        let some_duration = start.duration_since(std::time::UNIX_EPOCH).expect("Cannot get time");
        some_duration.as_secs() * 1000 + some_duration.subsec_millis() as u64
    }
}


pub use self::objects::*;
