use nphysics2d::{
    world::World,
    object::{
        BodyHandle,
        Material
    }
};
use nalgebra::{
    Point2,
    Point3,
    Vector2,
    Isometry2,
    Translation2,
    UnitComplex
};
use ncollide2d::{
    events::ContactEvent,
    shape
};
use std::rc::Rc;
use kiss3d::{
    text::Font,
    window::Window
};
use rand::{
    rngs::SmallRng,
    Rng,
    SeedableRng
};

use crate::crossplatform::{
    FPSCounter,
    play_sound
};
use crate::geometry::{
    Shape,
    get_tetros
};
use crate::objects::Obj;
use crate::types::N;
use crate::menu::Menu;

pub struct Game {
    objects: Vec<crate::objects::Obj>,
    pub physical_world: World<f32>,
    font: Rc<Font>,
    fps_counter: crate::crossplatform::FPSCounter,
    shapes: Vec<Shape>,
    rng: SmallRng
}

fn load_textures() -> Vec<&'static str> {
    let mut tex_names = Vec::new();
    macro_rules! generate_paths {
        ($($x: expr),*) => {{
            let mut v:Vec<(&[u8], &'static str)> = vec![];
            $( 
                v.push((include_bytes!(concat!("../static/", $x, ".png")), $x));
            )*
            v
        }};
    }

    for &(data, name) in generate_paths!("tex", "tex2").iter() {
        tex_names.push(name);
        kiss3d::resource::TextureManager::get_global_manager(|tm| tm.add_image_from_memory(data, name));
    }
    tex_names
}

impl Game {

    pub fn new() -> Game {
        load_textures();
        let mut physical_world = World::new();
        physical_world.set_gravity(Vector2::y() * -9.81);
        physical_world.set_timestep(0.05);
        Game {
            objects: vec![],
            physical_world,
            font: Font::from_bytes(include_bytes!("/usr/share/fonts/truetype/ubuntu/Ubuntu-R.ttf")).expect("Font load error"),
            fps_counter: FPSCounter::new(),
            shapes: get_tetros(),
            rng: SmallRng::from_seed([193, 84, 92, 255, 233, 124, 18, 186, 190, 106, 200, 22, 67, 124, 81, 119]), //fair python dice roll
        }
    }

    pub fn step(&mut self) {
        self.physical_world.step();
        for obj in self.objects.iter_mut() {
            obj.update(&self.physical_world);
        }
    }

    pub fn render(&mut self, w: &mut Window) {
        for e in self.physical_world.contact_events() {
            match e {
                ContactEvent::Started(..) => {
                    play_sound(0.1);
                },
                ContactEvent::Stopped(..) => {}
            }
        }
        let fps = self.fps_counter.get_fps();
        w.draw_text(
            &format!("FPS: {:.1}", fps),
            &Point2::new(10.0, 10.0),
            60.0,
            &self.font,
            &Point3::new(0.5, 0.5, 0.5)
        );
        self.fps_counter.frame();
    }

    pub fn add_floor(&mut self, w: &mut Window) {
        let mut floor = w.add_rectangle(1000.0, 5.0);
        let floor_transformation = Isometry2::from_parts(Translation2::new(0.0, -300.0), UnitComplex::identity());

        floor.set_color(0.3, 0.4, 0.5);
        floor.set_local_transformation(floor_transformation);

        let _ground_collider = self.physical_world.add_collider(
            0.06,
            shape::ShapeHandle::new(shape::Cuboid::new(Vector2::new(1000.0, 5.0))),
            BodyHandle::ground(),
            floor_transformation,
            Material::new(0.2, 0.5),
        );

    }

    pub fn add_object(&mut self, x: N, y: N, w: &mut Window) {
        let mut shape = self.rng.choose(&self.shapes).unwrap().clone();
        shape.scale(50.0);
        self.objects.push(
            Obj::new(
                &mut self.physical_world,
                w,
                x,
                y,
                self.rng.gen(),
                self.rng.gen(),
                self.rng.gen(),
                shape,
                self.rng.choose(&["tex", "tex2"]).unwrap()
            )
        );
    }
}
