#[cfg(target_arch = "wasm32")]
#[cfg_attr(target_arch = "wasm32", macro_use)]
extern crate stdweb;


use kiss3d::{
    light::Light,
    window::{State, Window},
    scene::PlanarSceneNode,
    text::Font};

mod types;
mod geometry;
mod objects;
mod crossplatform;
mod game;
mod menu;

use self::game::Game;
use self::menu::Menu;

pub trait Scene {
    fn step(&mut self) -> Option<Box<dyn Scene>>;
    fn render(&mut self, w: &mut Window);
}

struct AppState {
    scene: Box<dyn Scene>
}

impl State for AppState {
    fn step(&mut self, w: &mut Window) {
        self.scene.step();
        self.scene.render(w);

    }
}

fn main() {
    let mut window = Window::new("TETREZ");
    println!("Width {} Height {}", window.width(), window.height());
    let font = Font::from_bytes(include_bytes!("/usr/share/fonts/truetype/ubuntu/Ubuntu-R.ttf")).expect("Font load error");
    let mut state = AppState {
        scene: Box::new(Menu::new(&mut window, font.clone()))
    };

    window.set_light(Light::StickToCamera);
    window.render_loop(state)
}
